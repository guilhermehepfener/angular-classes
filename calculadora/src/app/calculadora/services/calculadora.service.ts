/** 
* Serviço responsável por executar as operações
* de uma calculadora
* @author Guilherme Hepfener
* @since v1.0.0
*/

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService {

  // Definição de constantes utilizadas nas 
  // operações de cálculo
  static readonly SOMA: string ='+';
  static readonly SUBTRACAO: string ='-';
  static readonly DIVISAO: string ='/';
  static readonly MULTIPLICACAO: string ='*';

  constructor() { }

  /**
   * Método de cálculo das operações
   * @param num1 number
   * @param num2 number
   * @param operacao String que caracteriza a operação
   * @return number resultado
   */

  calcular(num1: number, num2: number, operacao: string): number{
    let resultado: number;

    switch(operacao){
      case CalculadoraService.SOMA:
        resultado = num1 + num2;
      break;
      case CalculadoraService.SUBTRACAO:
        resultado = num1 - num2;
      break;
      case CalculadoraService.DIVISAO:
        resultado = num1 / num2;
      break;
      case CalculadoraService.MULTIPLICACAO:
        resultado = num1 * num2;
      break;
      default:
        resultado = 0;
    }
    return resultado;
  }
}
