import { Component, OnInit } from '@angular/core';

import { JogoDaVelhaService } from './shared';

@Component({
  selector: 'app-jogo-da-velha',
  templateUrl: './jogo-da-velha.component.html',
  styleUrls: ['./jogo-da-velha.component.css']
})
export class JogoDaVelhaComponent implements OnInit {

  constructor(private jogoDaVelhaService: JogoDaVelhaService) { }

  ngOnInit(): void {
    this.jogoDaVelhaService.inicializar();
  }

  /**
   * Retorna se a tela inicial deve ser exibido
   */
  get showInicio(): boolean{
    return this.jogoDaVelhaService.showInicio;
  }

  /**
   * Retorna se o tabuleiro deve ser exibido
   */
  get showTabuleiro(): boolean{
    return this.jogoDaVelhaService.showTabuleiro;
  }

  /**
   * Retorna se a tela final deve ser exibida
   */
  get showFinal(): boolean{
    return this.jogoDaVelhaService.showFinal;
  }

  /**
   * Inicializa os dados do jogo
   */
  iniciarJogo(): void{
    this.jogoDaVelhaService.iniciarJogo();
  }

  /**
   * Realiza uma jogada ao clicar
   * 
   * @param posX number
   * @param poxY number
   */
  jogar(posX: number, posY: number): void{
    this.jogoDaVelhaService.jogar(posX, posY);
  }

  /**
   * Retorna se a peça x deve ser exibida
   * para a coordenada informada
   * 
   * @param posX number
   * @param posY number
   * @returns 
   */
  exibirX(posX: number, posY: number): boolean{
    return this.jogoDaVelhaService.exibirX(posX, posY);
  }

  /**
   * Retorna se a peça o deve ser exibida
   * para a coordenada informada
   * 
   * @param posX number
   * @param posY number
   * @returns 
   */
  exibirO(posX: number, posY: number): boolean{
    return this.jogoDaVelhaService.exibirO(posX, posY);
  }

  /**
   * Retorna se a vitória deve ser exibida
   * para a coordenada informada
   * 
   * @param posX number
   * @param posY number
   * @returns 
   */
  exibirVitoria(posX: number, posY: number): boolean{
    return this.jogoDaVelhaService.exibirVitoria(posX, posY);
  }
  /**
   * retorna o jogador da vez
   * 
   * @returns number
   */
  get jogador(): number{
    return this.jogoDaVelhaService.jogador;
  }

  /**
   * Novo jogo
   */
  novoJogo(): void{
    this.jogoDaVelhaService.novoJogo();
  }

}
