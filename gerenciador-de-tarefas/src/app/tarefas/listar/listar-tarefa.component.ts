import { Component, OnInit } from '@angular/core';
import { TarefaService, Tarefa } from '../shared';

@Component({
  selector: 'app-listar-tarefa',
  templateUrl: './listar-tarefa.component.html',
  styleUrls: ['./listar-tarefa.component.css']
})
export class ListarTarefaComponent implements OnInit {

  tarefas: Tarefa[];

  constructor(private tarefaService: TarefaService) { }

  ngOnInit(): void {
    this.tarefas = this.listarTodos();
  }

  /**
   * Método que reaproveita o "listar" do diretório shared
   * 
   * @return Tarefa[]
   */
  listarTodos(): Tarefa[]{
    return this.tarefaService.listarTodos();
  }

  /**
   * Métoro que remove a tarefa
   * 
   * @param $event any
   * @param tarefa Tarefa
   */
  remover($event: any, tarefa: Tarefa): void{
    $event.preventDefault();
    if(confirm('Deseja remover a tarefa "' + tarefa.nome +'"?')){
      this.tarefaService.remover(tarefa.id);
      this.tarefas = this.listarTodos();
    }
  }

  /**
   * Método para alterar o status da tarefa
   * 
   * @param tarefa tarefa
   */
  alterarStatus(tarefa: Tarefa): void{
    if(confirm('Deseja alterar o status da tarefa "' + tarefa.nome + '"?')){
      this.tarefaService.alterarStatus(tarefa.id);
      this.tarefas = this.listarTodos();
    }
  }

}
